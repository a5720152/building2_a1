from django.conf.urls import url

from . import views

app_name = 'vocab'
urlpatterns = [
    url(r'^$', views.Index, name='index'),
    url(r'^viewall/', views.Viewall.as_view(), name='viewall'),
    url(r'^search/', views.Search, name='search'),
    url(r'^detail/(?P<search_text>[\w-]+)/$', views.Detail, name='detail'),
    url(r'^add/', views.add, name='add'),
    url(r'^backup_page/$', views.backup_page, name='backup_page'),   
    url(r'^import/$', views.restoreCSV, name='restoreCSV'),
  
    url(r'^searasdfch/', views.historylink, name='historylink'), 

]
