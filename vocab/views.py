from django.shortcuts import render,render_to_response
from django.views import generic
from .models import Word, Mean, Search_History
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
import time
import datetime
import csv
import os
from django.contrib import messages
from django.core.files.storage import default_storage
import xml.etree.cElementTree as ET
from xml.etree.ElementTree import ElementTree
from django.utils import timezone
from django.shortcuts import render,get_object_or_404


def Index(request):
    search_list = Search_History.objects.order_by('-lasted_used')
    return render(request,'vocab/index.html',{'search_list' : search_list })

class Viewall(generic.ListView):
    template_name = 'vocab/viewall.html'
    context_object_name = 'word_list'
    def get_queryset(self):
        return Word.objects.order_by('word_text')

def Detail(request,search_text):
    word = Word.objects.filter(word_text__startswith=str(search_text))
    return render_to_response('vocab/detail.html', {'wordlist' : word})

def Search(request):
    search_text = request.POST.get('search_text', None)
    if Search_History.objects.filter(search_text=search_text):
        search_use = get_object_or_404(Search_History,search_text=search_text)
        search_use.lasted_used=timezone.now()
        search_use.count+=1
        search_use.save()
    else:
        search_use  = Search_History(search_text=search_text,lasted_used= timezone.now() )
        search_use.save()

    wordlist = Word.objects.filter(word_text__startswith=str(search_text)).order_by('word_text')
    
    return render_to_response('vocab/search.html', {'wordlist' : wordlist , 'search_text' :search_text} )

def historylink(request):
    searchtext= search.search_text
    wordlist = Word.objects.filter(word_text__startswith=str(searchtext)).order_by('word_text')
    
    return render_to_response('vocab/search.html', {'wordlist' : wordlist , 'search_text' :searchtext} )



def add(request):
    error_message = '' 
    if request.method == 'POST':
        word_text_form = request.POST['word']
        meaning_text_form = request.POST['meaning']
        meaning_type_text_form = request.POST['meaning_type']
        sample_sentence_text_form = request.POST['sample_sentence']

        if word_text_form !='' and meaning_text_form != '' and meaning_type_text_form != '' and sample_sentence_text_form != '': 
            if Word.objects.filter(word_text=word_text_form):  # check objects same name
                word = Word.objects.get(word_text=word_text_form)
                if not word.mean_set.filter(mean_text=meaning_text_form):  # check same mean
                    word.mean_set.create(mean_text=meaning_text_form,type_text=meaning_type_text_form,sample=sample_sentence_text_form) #Create new meaning
                    backupCSV(request)# Update to csv
                    backupXML(request)# Update to XML
                    error_message = 'Update Word Complete'
                else:
                    error_message = 'This meaning already in this word'

            else:  # not same name can create new word
                word = Word(word_text=word_text_form)
                word.save()
                word.mean_set.create(mean_text=meaning_text_form,type_text=meaning_type_text_form,sample=sample_sentence_text_form)
                error_message = 'Update Word Complete'
                backupCSV(request) # Update to csv
                backupXML(request)# Update to XML
        else:
            print('fill not complete')
            error_message = 'fill not complete'

  
    context = {'error_message':error_message}
    return render(request,'vocab/add.html',context)



def backup_page(request):
    backupXML(request)
    backupCSV(request)
    return render(request,'vocab/backup.html')


def backupCSV(request):
    all_word=Word.objects.all()
    cols = ['word','mean','type','sample']
    print(time.localtime())
    output = csv.DictWriter(open('vocab/backup/CSV_'+datetime.datetime.now().strftime("%d:%m:%y[%H:%M]")+'.csv','wt'), fieldnames=cols)
    output.writeheader()
    for word in all_word:
        output.writerow({'word':word.word_text})
        for mean in word.mean_set.all():
            output.writerow({'word': '',  'mean':mean.mean_text,'type': mean.type_text,'sample': mean.sample})

def restoreCSV(request):
    dirPath = 'vocab/backup/'
    if request.FILES != {}:  # Check file isn't empty
        csvFile = request.FILES['Backup_File']
        saveFilePath = default_storage.save(dirPath,csvFile)
        os.rename(saveFilePath,'{}{}'.format(dirPath,csvFile.name))
        all_word = Word.objects.all()
        for word in all_word:
            word.delete()  # clear old word
        with open('{}{}'.format(dirPath,csvFile.name)) as csvfile:        #with csvfile:

            reader = csv.DictReader(csvfile)
            for row in reader:
                if row['word'] != '':
                    if Word.objects.filter(word_text=row['word']):
                        word = Word.objects.get(word_text=row['word'])
                    else:
                        word = Word(word_text=row['word'])
                        word.save()
                else:
                    if not word.mean_set.filter(mean_text=row['mean']):  # if not have same meaning , create new meaning
                        word.mean_set.create(mean_text=row['mean'], type_text=row['type'], sample=row['sample'])

        return HttpResponseRedirect(reverse('vocab:viewall'))


def backupXML(request):
    all_word = Word.objects.all()
    root = ET.Element("vocab")
    for each_word in all_word:
        word = ET.SubElement(root,"word",name=each_word.word_text)
        for each_mean in each_word.mean_set.all():
            mean = ET.SubElement(word, "mean",name=each_mean.mean_text)
            ET.SubElement(mean, "type_text").text = each_mean.type_text
            ET.SubElement(mean, "sample").text = each_mean.sample

    tree = ET.ElementTree(root)
    tree.write('vocab/backup/XML_'+datetime.datetime.now().strftime("%d:%m:%y[%H:%M]")+'.xml')

def restoreXML(request):
    dirPath = 'vocab/backup/'
    if request.FILES != {}:  # Check file isn't empty
        xmlFile = request.FILES['Backup_File']
        saveFilePath = default_storage.save(dirPath,xmlFile)
        os.rename(saveFilePath,'{}{}'.format(dirPath,xmlFile.name))
        all_word = Word.objects.all()
        for word in all_word:
            word.delete()  # clear old word
        tree = ElementTree()
        tree.parse('{}{}'.format(dirPath,xmlFile.name))
        root = tree.getroot()
        for word in root:
            print(word.tag)
            if Word.objects.filter(word_text=word.name):
                 word = Word.objects.get(word_text=word.name)
            else:
                 word = Word(word_text=word.name)
                 word.save()
            for mean in word:
                if not word.mean_set.filter(mean_text=mean.get('name')):  # if not have same meaning , create new meaning
                    word.mean_set.create(mean_text=mean.get('name'), type_text=mean.find('type_text').text, sample=mean.find('sample').text)

        return HttpResponseRedirect(reverse('vocab:viewall'))    
        
        






