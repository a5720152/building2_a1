from django.contrib import admin
from .models import Word, Mean

class MeanInline(admin.TabularInline):
    model = Mean


class WordAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['word_text']}),
    ]
    inlines = [MeanInline]


admin.site.register(Word, WordAdmin)

