from django.db import models
from django.utils import timezone
import datetime
class Word(models.Model):
    word_text = models.CharField(max_length=100)

    def __str__(self):
        return self.word_text

class Mean(models.Model):
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    mean_text = models.CharField(max_length=200)
    type_text = models.CharField(default='Unknown',max_length=20)
    sample = models.CharField(default='None',max_length=200)
    def __str__(self):
        return self.mean_text


class Search_History(models.Model):
    search_text= models.CharField(max_length=200)
    lasted_used= models.DateTimeField('date published')
    count = models.IntegerField(default=1)
    
    def __str__(self):
        return self.search_text

